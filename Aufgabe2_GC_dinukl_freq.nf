// Eingabe: FASTA-Datei 
// GC content berechnen: (G+C) / (A+C+G+T)
// Zusatzaufgabe: Dinukleotidfrequenzen berechnen 
// Zusatzaufgabe: Gesamt-Ausgabedatei mit GC content und Dinukleotidfrequenzen untereinander
nextflow.enable.dsl=2

process read_fasta{
    publishDir "/tmp/fastaGC", mode: 'copy', overwrite: true // die ergebnisse vom output werden hier rein kopiert
    input:
        path infile // über stelle definiert
    output:
        path "${infile}.line*", emit: eingabedatei 
    script: // alles hier und folgende zeile ist bash /* mit d zeichen kann man auf variable greifen. */ 
        """
        split -l 2 ${infile} -d ${infile}.line #// jeweils 2 zeilen
        """
}

process content_GC {
  publishDir params.outdir, mode: 'copy', overwrite: true
  input:
    path infile
  output:
    path "${infile}.content_GC", emit: GCcontent
  script:
    """
    python3 /home/anke/Desktop/Weiterbildung/modul3/ngsmodule/eigens_git/contentGC.py ${infile} > ${infile}.content_GC
    """
}



workflow {
    inchannel = channel.fromPath("../cq-init/data/sequences.fasta")
    splitfiles = read_fasta(inchannel) // name des ersten prozesses
    GC_content = content_GC(splitfiles.flatten())
    //stopcounts = count_Stop(splitfiles.flatten())
    //allStops = sumAllStops(stopcount.collect())
}




