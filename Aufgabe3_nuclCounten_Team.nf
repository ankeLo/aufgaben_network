// Aufgabe 3 nextflow - pipline die fastadatei einliest und die jeweilige sequenzlänge der Segenzen ausgibt
// entweder mit python oder bash tail -n 1 
// Datei.fasta > nursequenz 
// wc -m = sagt wieviele zeichen in einer datei sind
// input: fasta
// output: länge der sequenzen

nextflow.enable.dsl=2

process split_file{

    input:
        path infile
    output:
        path "${infile}.seq*"
    script: // """ definiert einen string, wird aber script variable verwendet
    """ 
    split -l 2 ${infile} -d ${infile}.seq #// .seq ist nur der name, welche hinten angehänt wird
    """
}

// laengen berechnen
process calc_length{
    input:
        path seqfile // hier bennen wir den channel erst 
    output:
        path "${seqfile}.laenge" 
    script:
    """
    tail -n 1 ${seqfile} > sequenze
    cat sequenze | tr -d "\n" | wc -m > ${seqfile}.laenge
    """
    // wc -m zähl die zeichen
   // tr -d "\n" = tr ersetzt textzeichen mit -d diese löschen
   // letzte zeichen löschen, da sonst als base gezähl

}


process summary{
    publishDir "${params.outdir}", mode: 'copy', overwrite: true
    input:
        path lenfiles
    output:
        path "lenout"
    script:
    """
    cat ${lenfiles} > lenout
    """
// cat inhalt anzeigen
// ${lenfiles} > lenout
}



workflow{

// einlesen:
    inchannel = channel.fromPath(params.infile) // beim aufruf in der konsole den pfad als --infile "Pfad angeben"

// split_file:
    splitfiles = split_file(inchannel) // hier kommt alles als ein batzen (array) raus

// laenge bestimmen:
    laengen = calc_length(splitfiles.flatten()) // ohne flatten() wird einmal das array eingelesen und fertig, 
                                                // nicht die dateien nacheinander. mit flatten() aufruf je datei

// zusammenfassung der ergebnisse:
    ergebnisse = summary(laengen.collect())

}






















